import React from "react";
import ReactDOM from "react-dom";

// include your styles into the webpack bundle
import "./styles/index.css";
import App from "./App.js";

//import your own components
//import Home from "./js/component/tarea.jsx";

//render your react application
ReactDOM.render(<App />, document.querySelector("#app"));
