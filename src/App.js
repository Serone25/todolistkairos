import React from "react";
import ListaDeTareas from "./js/component/listaDeTareas.jsx";


import "./styles/App.css";
import "./styles/Tarea.css"


function App(){
    return (
        <div className="aplicacion-tareas">
            <div className="tareas-lista-principal">
                <h1>Mis Tareas</h1>
                <ListaDeTareas/>
            </div>
        </div>
    )
}

export default App;
