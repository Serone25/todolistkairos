FROM node:18.0.0-alpine as build
WORKDIR /app
COPY . .
EXPOSE 3000

RUN npm install --silent
RUN npm install react-scripts@5.0.1 -g --silent

RUN npm run build

FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
#RUN rm /etc/ngnix/conf.d/default.conf
COPY nginx/default.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
